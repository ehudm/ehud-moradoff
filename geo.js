//global variables for fields
    var lat = document.getElementsByName('latitude');
    var lon = document.getElementsByName('longitude');
    var accuracy = document.getElementsByName('accuracy');
    var altAccuracy = document.getElementsByName('altitudeAccuracy');
    var altitude = document.getElementsByName('altitude');  
    var speed = document.getElementsByName('speed');    
    var heading = document.getElementsByName('heading');
    var coordinates = document.getElementsByName('coords');
    var code = document.getElementsByName('errorCode'); 
    var codeMsg = document.getElementsByName('errMsg'); 
    var time = document.getElementsByName('timestamp'); 
     
    var watchId;
 
function getLocation(){
	
 if (navigator.geolocation)
    {
    navigator.geolocation.getCurrentPosition(getGeographicalPostion,
                                             errorHandler,
                                             {maximumAge:600000});
     
    }
  else{codeMsg.value="Geolocation is not supported by this browser.";
  }
  }
   
function getGeographicalPostion(position)
  {
     
     lat[0].value = position.coords.latitude; 
     lon[0].value = position.coords.longitude; 
     accuracy[0].value = position.coords.accuracy; 
     altAccuracy[0].value =position.coords.altitudeAccuracy; 
     altitude[0].value = position.coords.altitude; 
     speed[0].value = position.coords.speed; 
     heading[0].value = position.coords.heading; 
     time[0].value = position.timestamp; 
}
 
function errorHandler(error){
     
    code[0].value = error.code; 
     
}
 
function startWatchPositon(){
    watchId = navigator.startWatch(getGeographicalPostion,
                                             errorHandler);
     
}
 
function stopWatchPosition(watchId){
    navigator.geolocation.clearWatch(watchId);
}