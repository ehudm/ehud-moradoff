var geocoder;
var service;
var map;
var markers = Array();
var infos = Array();
var request;
var addrLocation;

function initialize() {
    // prepare Geocoder
    geocoder = new google.maps.Geocoder();
    // set initial position (New York)
    var pyrmont = new google.maps.LatLng(-33.8665433,151.1956316);

    var myOptions = { // default map options
        zoom: 14,
        center: pyrmont,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
    
    
}



// find address function
function findAddress() {
 var address = document.getElementById("gmap_where").value;

    // script uses our 'geocoder' in order to find location by address name
    geocoder.geocode( { 'address': address}, function(results, status)
     {
        if (status == google.maps.GeocoderStatus.OK) { // and, if everything is ok

            // we will center map
             addrLocation = results[0].geometry.location;
          
            map.setCenter(addrLocation);
            document.getElementById('lat').value = results[0].geometry.location.Xa;
            document.getElementById('lng').value = results[0].geometry.location.Ya;
		    var request = {
           location: addrLocation,
            radius: '100'//,
            // types: ['store']
            };
           service = new google.maps.places.PlacesService(map);
           service.nearbySearch(request, callback1);
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
    
}

 function callback1(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      var place = results[i];
            
      var request = {
          reference: place.reference
      };
    service = new google.maps.places.PlacesService(map);
    service.getDetails(request, callback2);
    }}
  
}
function callback2(place, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
  	if (place.geometry.location.equals(addrLocation))
     {
     
      createAddressMarker(place);
	  createCurrierMarker(place);
    }
  }
}
 function createAddressMarker(place) {
        var placeLoc = place.geometry.location;
        
       var infowindow ;

        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location,
icon: 'marker.png',
title: place.name + " "+ place.formatted_address + " "+ place.formatted_phone_number  
        });
 
    
  }
       
      
    function createCurrierMarker(place)
    {
    	//32.27458, 34.85884199999998
    	var lat = 32.27698//document.getElementById('lat').value;
    var lng = 34.85884199999998//document.getElementById('lng').value+50;
    var cur_location = new google.maps.LatLng(lat, lng);
    
    	var marker = new google.maps.Marker({
          map: map,
          position:cur_location,
//icon: 'img/deliver.jpg',
          icon: 'marker.png',
          title: 'John SCholnik'     
        });
    	
    	 google.maps.event.addListener(marker, 'click', function() {
         infowindow = new google.maps.InfoWindow({
        map: map,
        position: place.geometry.location,
        content:'John SCholnik' + '<button id ="btn_choose_currier" onclick = "choose_currier()">Choose Him</button>'
              });
       
          infowindow.open(map, this);
        });
    } 
    
     
    function choose_currier()
    {
    	alert('dddd');
    }

// initialization
google.maps.event.addDomListener(window, 'load', initialize);

/* Author:Noga Pour 

*/























